import time
import argparse

from Tsp import *


def print_stats(title, tsp_file, cycle, score, start, end):
    print(title)
    print('Cycle:', cycle)
    print('Cycle length:', len(cycle))
    print('File dimension:', tsp_file.dimension)
    print('Score:', score)
    print(f'Time: {(end - start) * 1000:.2f} ms')
    # print("Valid?", tsp_file.is_valid_cycle(cycle))
    print('\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='TSP Greedy Algorithm')
    parser.add_argument('-f', '--file', help='TSP file', required=True)
    parser.add_argument('-s', '--start-node', type=int, help='Starting node', required=False, default=0)
    parser.add_argument('-k', '--k-neighbours', type=int, help='K neighbours for third random algorithm',
                        required=False, default=3)
    args = parser.parse_args()

    tsp_instance = TspInstance.from_file(args.file)
    tsp = Tsp(tsp_instance)

    start = time.time()
    cycle, score = tsp.greedy_tsp1(args.start_node)
    end = time.time()
    print_stats('Greedy TSP 1 :', tsp_instance, cycle, score, start, end)

    start = time.time()
    cycle, score = tsp.greedy_tsp2_with_pairs(args.start_node)
    end = time.time()
    print_stats('Greedy TSP 2 (with pairs):', tsp_instance, cycle, score, start, end)

    start = time.time()
    cycle, score = tsp.greedy_tsp3_with_k_neighbours(args.start_node, args.k_neighbours)
    end = time.time()
    print_stats('Greedy TSP 3 (random between the k nearest nodes neighbours):', tsp_instance, cycle, score, start, end)
