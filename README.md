# GloutonForTheCommerceTraveler

## Usage of the project:

```
usage: main.py [-h] -f FILE [-s START_NODE] [-k NUMBER_NEAREST_NEIGHBORS]

TSP Greedy Algorithm by Maxime VITSE and Axel LEBAS

optional arguments:
  -h, --help                                                    Show this help message
  -f FILE, --file FILE                                          TSP file
  -s START_NODE, --start-node START_NODE                        Starting node
  -k NUMBER_NEAREST_NEIGHBORS, --k-NUMBER_NEAREST_NEIGHBORS     K neighbours for third random algorithm
```

Sample TSP files are located in the [instances_TSP](./instances_TSP) folder

The TSP file should have the `NAME`, `TYPE`, `COMMENT`, `DIMENSION`, `EDGE_WEIGHT` properties **in that order** *(all the sample files provided by Sara Tari follow that rule)*

## Additional informations

Greedy algorithm for the [Travelling salesman problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem)

[PDF file with the project questions by Sara Tari](./docs/AA_CC2_projet1_I2L.pdf)

### Pair programming tools:

- <ins>Jamboard</ins>
- <ins>Replit:</ins> https://replit.com/@alebas/GloutonForTheCommerceTraveler
- <ins>Discord</ins>

### Authors:

- Axel LEBAS
- Maxime VITSE

(in M1 I2L APP 2021-2022)
