import random
from dataclasses import dataclass


@dataclass
class TspInstance:
    """
        Class for representing a tsp instance file
    """
    name: str
    file_type: str
    comment: str
    dimension: int
    edge_weight_type: str
    edge_weight_format: str
    edge_weight_matrix: list

    def is_valid_cycle(self, cycle):
        return cycle[0] == cycle[-1] and len(cycle) - 1 == self.dimension

    @staticmethod
    def from_file(file_path: str) -> 'TspInstance':
        try:
            # Open the file by its file path
            with open(file_path, 'r') as f:
                def get_next_line_attribute():
                    # Get the next line and split it by whitespace
                    return f.readline().strip().split()[1]

                name = get_next_line_attribute()  # NAME
                file_type = get_next_line_attribute()  # TYPE
                comment = get_next_line_attribute()  # COMMENT
                dimension = int(get_next_line_attribute())  # DIMENSION
                edge_weight_type = get_next_line_attribute()  # EDGE_WEIGHT_TYPE
                edge_weight_format = get_next_line_attribute()  # EDGE_WEIGHT_FORMAT
                edge_weight_matrix = reshape_matrix_by_dimension(f.read().split()[1:], dimension)  # EDGE_WEIGHT_SECTION
                # edge_weight_matrix = reshape_matrix_by_dimension_with_numpy(f.read().split()[1:-1], dimension)
                # EDGE_WEIGHT_SECTION
            return TspInstance(name, file_type, comment, dimension, edge_weight_type, edge_weight_format,
                               edge_weight_matrix)
        except Exception as e:
            raise SystemExit(e)


def reshape_matrix_by_dimension(edge_weight_section: list, dimension: int) -> list:
    """
    Reshape the edge_weight_section to a square matrix
    :param edge_weight_section: the edge_weight_section or the distance matrix but as a list
    :param dimension: dimension of the final square matrix
    :return:
    """
    edge_weight_matrix = []
    matrix_line = []

    index = 0

    # Browse the edge_weight_section line by line
    for distance_value in edge_weight_section:
        if index == dimension:
            edge_weight_matrix.append(matrix_line)
            # Reset matrix line
            matrix_line = []
            index = 0

        # If we are at the end of the line, add the last sub_arr
        if distance_value != 'EOF':
            matrix_line.append(int(distance_value))

        index += 1

    assert is_square_matrix(edge_weight_matrix), 'The given matrix isn\'t a square matrix'
    return edge_weight_matrix


def is_square_matrix(matrix: list) -> bool:
    return len(matrix) == len(matrix[len(matrix) - 1])


@dataclass
class Tsp:
    """
        Class representing a traveling salesmen problem
    """
    file: TspInstance = None

    def greedy_tsp1(self, start_node: int) -> tuple[list[int], int]:
        """
        Greedy algorithm to solve the traveling salesmen problem
        :param start_node: the starting node
        :return: the cycle and its score
        """

        self.check_start_node(start_node)

        distance_matrix = self.file.edge_weight_matrix

        # Initialize the cycle
        cycle = [start_node]

        visited = [start_node]
        unvisited = [node for node in range(self.file.dimension) if node is not start_node]
        current_node = start_node

        # while there are unvisited nodes (constraint of the tsp)
        while len(unvisited) > 0:
            # Find the closest unvisited node
            # the lambda function is used to find the minimum distance between the current node and the unvisited nodes
            next_node = minimum_next_node(distance_matrix, current_node, unvisited)
            # Add the closest node to the cycle
            cycle.append(next_node)
            # Add the closest node to the visited nodes
            visited.append(next_node)
            # Remove the closest node from the unvisited list
            unvisited.remove(next_node)
            # Set the current node to the closest node
            current_node = next_node

        # Add the first node to the cycle to complete the cycle
        cycle.append(start_node)

        return cycle, get_score_of_cycle(cycle, distance_matrix)

    def check_start_node(self, start_node):
        if start_node > self.file.dimension - 1:
            raise SystemExit(f'The start node is out of range. The starting point should be inferior or equal to '
                             f'{self.file.dimension - 1}')

    def greedy_tsp2_with_pairs(self, start_node: int) -> tuple[list[int], int]:
        """
        Greedy algorithm to solve the traveling salesmen problem
        :param start_node: The starting node
        :return: the cycle and its score
        """

        def minimum_value_index(distance_matrix: list, unvisited_nodes: list) -> tuple[int, int]:
            """
            Find the minimum value index in the distance matrix
            :param unvisited_nodes:
            :param distance_matrix: the distance matrix
            :return: the minimum value index
            """
            min_value = -1
            min_value_index = tuple()

            # Browse the distance matrix line by line
            for i in range(len(distance_matrix)):
                for j in range(len(distance_matrix[i])):
                    # If i and j are still the unvisited
                    if i in unvisited_nodes and j in unvisited_nodes:
                        # If the value is inferior to the current min value
                        if min_value == -1 or distance_matrix[i][j] < min_value:
                            min_value = distance_matrix[i][j]
                            min_value_index = (i, j)

            return min_value_index

        def generate_pairs_v2(distance_matrix: list, unvisited_nodes: list) -> list[tuple[int, int]]:
            # Initialize the generated pairs list to return it at the end
            pairs_generated = []

            # While there are still unvisited nodes
            while unvisited_nodes:
                # Find the minimum value indexes
                node_left, node_right = minimum_value_index(distance_matrix, unvisited_nodes)

                # print("nodes", node_left, node_right)
                # print("distance min", distance_matrix[node_left][node_right])

                # Add the pair to the generated pairs list
                pairs_generated.append((node_left, node_right))

                # print("unvisited_nodes", unvisited_nodes)
                # print(f'Removing {node_left} and {node_right} from the unvisited nodes list')

                # remove the nodes from the unvisited nodes list
                if node_left != node_right:
                    unvisited_nodes.remove(node_left)
                    unvisited_nodes.remove(node_right)
                else:
                    unvisited_nodes.remove(node_left)

                # print("unvisited_nodes after removing", unvisited_nodes)

            return pairs_generated

        def generate_pairs(distance_matrix: list, unvisited_pairs: list) -> list[tuple[int, int]]:
            """
            !Deprecated!
            Generate all the pairs of nodes with a minimum distance
            :param distance_matrix:
            :param unvisited_pairs:
            :return:
            """

            # copy the unvisited pairs to avoid modifying the original list to know all pairs
            unvisited_pairs_copy = unvisited_pairs[:]

            # Initialize the generated pairs list to return it at the end
            pairs_generated = []

            # While there are unvisited pairs
            for node_left in unvisited_pairs:
                # double check if the node_left is in the unvisited pairs
                if node_left in unvisited_pairs_copy:
                    # finding the closest node to the node_left
                    node_right = minimum_next_node(distance_matrix, node_left, unvisited_pairs_copy)
                    # add the pair to the generated pairs list
                    pairs_generated.append((node_left, node_right))

                    # a pair has been generated, remove it from the unvisited pairs
                    # a pair can have to two same nodes if no other pair can be generated (odd number of nodes)
                    # so we remove treat differently these two cases
                    if node_left != node_right:
                        unvisited_pairs_copy.remove(node_left)
                        unvisited_pairs_copy.remove(node_right)
                    else:
                        unvisited_pairs_copy.remove(node_left)
            return pairs_generated

        self.check_start_node(start_node)

        unvisited = [node for node in range(self.file.dimension) if node is not start_node]

        # Initialize the cycle with the start node and the closest node to the start node (the first pair to handle
        # the start node if it's different from 0)
        next_node = minimum_next_node(self.file.edge_weight_matrix, start_node, unvisited)
        cycle_start = [start_node, next_node]
        cycle = []
        unvisited.remove(next_node)

        # Generating the pairs of nodes with a minimum distance
        pairs = generate_pairs_v2(self.file.edge_weight_matrix, unvisited.copy())

        # Generate the cycle with the generated pairs
        for pair in pairs:
            node_1, node_2 = pair

            # test the two cases of insertion of the pair in the cycle (before or after)
            temp_insert_before = cycle.copy()
            temp_insert_after = cycle.copy()

            if node_1 != node_2:
                # in both cases, we need to remove the nodes from the unvisited list
                unvisited.remove(node_1)
                unvisited.remove(node_2)

                # insert the pair in a temporary cycle at the beginning of the cycle
                temp_insert_before[0:0] = [node_1, node_2]

                # insert the pair in a temporary cycle at the end of the cycle
                temp_insert_after.extend([node_1, node_2])
            else:
                # we handle the case of a pair with the same node (odd number of nodes)

                # in both cases, we need to remove the nodes from the unvisited list
                unvisited.remove(node_1)

                # insert the pair in a temporary cycle at the beginning of the cycle
                temp_insert_before[0:0] = [node_1]

                # insert the pair in a temporary cycle at the end of the cycle
                temp_insert_after.extend([node_1])

            # check which cycle is the best between the two cases (inserting at the beginning or at the end)
            if get_score_of_cycle(temp_insert_before, self.file.edge_weight_matrix) < get_score_of_cycle(
                    temp_insert_after, self.file.edge_weight_matrix):
                cycle = temp_insert_before
            else:
                cycle = temp_insert_after

        cycle[0:0] = cycle_start
        cycle.append(start_node)

        return cycle, get_score_of_cycle(cycle, self.file.edge_weight_matrix)

    def greedy_tsp3_with_k_neighbours(self, start_node: int, k: int) -> tuple[list[int], int]:
        """
        Greedy algorithm to solve the traveling salesmen problem
        :param start_node: The starting node
        :param k: the number of neighbours nodes to visit around the current node
        :return: the cycle and its score
        """

        self.check_start_node(start_node)

        distance_matrix = self.file.edge_weight_matrix

        # Initialize the cycle
        cycle = [start_node]

        visited = [start_node]
        unvisited = [node for node in range(self.file.dimension) if node is not start_node]
        current_node = start_node

        # while there are unvisited nodes (constraint of the tsp)
        while len(unvisited) > 0:
            # Find the closest unvisited node
            # the lambda function is used to find the minimum distance between the current node and the unvisited nodes
            next_node = choose_random_node_limited_on_the_k_nearest(distance_matrix, current_node, unvisited, k, False)
            # Add the closest node to the cycle
            cycle.append(next_node)
            # Add the closest node to the visited nodes
            visited.append(next_node)
            # Remove the closest node from the unvisited list
            unvisited.remove(next_node)
            # Set the current node to the closest node
            current_node = next_node

        # Add the first node to the cycle to complete the cycle
        cycle.append(start_node)

        return cycle, get_score_of_cycle(cycle, distance_matrix)


def minimum_next_node(distance_matrix, current_node, unvisited):
    """
    Find the closest unvisited node to the current node
    :param distance_matrix: the distance matrix
    :param current_node: current node to find the closest unvisited node
    :param unvisited: unvisited nodes
    :return: closest node
    """
    min_node = min(unvisited, key=lambda x: distance_matrix[current_node][x])
    return min_node


def choose_random_node_limited_on_the_k_nearest(distance_matrix, current_node, unvisited, k: int, version2: bool):
    """
    Choose a random node from the k nearest nodes
    :param k:
    :param distance_matrix:
    :param current_node:
    :param unvisited:
    :param version2: activate or not the version 2 of the k nearest algorithm (minimum next nodes or random if len(unvisited) < k)
    :return: a random node from the k nearest nodes
    """
    if (len(unvisited)) >= k:
        # Get the k nearest nodes
        nearest_nodes = []
        tmp_unvisited = unvisited.copy()
        # print("tmp_unvisited: ", tmp_unvisited)
        # print("unvisited: ", unvisited)
        for i in range(k):
            next_node = minimum_next_node(distance_matrix, current_node, tmp_unvisited)
            nearest_nodes.append(next_node)
            tmp_unvisited.remove(next_node)

        # print("nearest_nodes: ", nearest_nodes)
        # Choose a random node among the k nearest nodes
        random_node = random.choice(nearest_nodes)
    else:
        if version2 is True:
            random_node = random.choice(unvisited)
        else:
            random_node = minimum_next_node(distance_matrix, current_node, unvisited)
    # print("random_node: ", random_node)
    return random_node


def get_score_of_cycle(cycle, distance_matrix):
    return sum(distance_matrix[cycle[i]][cycle[i + 1]] for i in range(len(cycle) - 1))


def get_detailed_distances_of_cycle(cycle, distance_matrix):
    for i in range(len(cycle) - 1):
        print(f'{cycle[i]} to {cycle[i+1]}, distance = {distance_matrix[cycle[i]][cycle[i+1]]}')
